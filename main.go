package main

import (
	"log"
	"net"
	"net/http"

	"github.com/gorilla/mux"
)

func main() {
	log.Println("Initializing Dog-Seeker API")

	router := mux.NewRouter()
	router.HandleFunc("/", ServeHTTP)

	server := http.Server{
		Addr:    net.JoinHostPort("", "6400"),
		Handler: router,
	}

	err := server.ListenAndServe()
	if err != nil {
		log.Fatal(err)
	}
}

func ServeHTTP(w http.ResponseWriter, r *http.Request) {
	// Ecrire dans le header de la réponse le code 200
	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json")

	// Ecrire dans le body un message json
	_, err := w.Write([]byte(`{"message", "Dog Seeker"}`))
	if err != nil {
		log.Fatal(err)
	}
}
